<?php

namespace App\app;

use Illuminate\Database\Eloquent\Model;

class Addresses extends Model
{
    //
    protected $fillable = [
        'user_id', 'zipcode', 'location', 'number', 'neighborhood', 'city', 'state',
    ];
    
    function user(){
        return this->belongsTo(User::class, 'user_id', 'id');
    }
}
