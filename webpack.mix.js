const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */



mix.js([
    'resources/assets/js/site.js'
], 'public/assets/js/site.js')

mix.styles([
    'resources/assets/css/site.css'
], 'public/assets/css/site.css')

mix.copy([    'resources/assets/image/site'], 'public/assets/image/site')

